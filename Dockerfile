FROM devops.neutrona.com:5000/base

RUN mkdir -p /src
ADD . /src

WORKDIR /src
RUN npm install

ENV DEBUG *
CMD nodejs .

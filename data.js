var shortid = require('shortid');
var config = require('config');

var debug = require('debug')('origami:plugin:Data');

function callbackToPromise(resolve, reject) {
  return function (err, result) {
    if (err) return reject(err);

    resolve(result);
  };
}

function Data (db, fold) {
  this.db = db;
  this.fold = fold;
}

Data.prototype.getCollections = function () {
  var self = this;

  debug('.getCollections');

  return new Promise(function (resolve, reject) {
    self
    .db
    .collection('collections').find({
      fold: self.fold
    }).toArray(callbackToPromise(resolve, reject));
  });
};

Data.prototype.dropCollection = function (name) {
  var self = this;
  debug('dropCollection %s', name);

  return new Promise(function (resolve, reject) {
    function removeFromList(callback) {
      self.db.collection('collections').remove({
        fold: self.fold,
        name: name
      }, function (err) {
        if (!err) debug('Collection %s removed from list', name);
        if (err) return callback(err);

        //FIXME: self.announce('collection-drop', name);

        return callback();
      });
    }

    self
    .db
    .collection('collections').findOne({
      fold: self.fold,
      name: name
    }, function (err, doc) {
      if (err) return callback(err);

      if (!doc) {
        debug('Collection %s not found', name);

        //FIXME: self.announce('collection-drop', name);

        return resolve();
      }

      if (!doc.as) return removeFromList(callbackToPromise(resolve, reject));

      debug('%s being backed as %s', name, doc.as);

      self.db.collection(doc.as).drop (function (err) {
        if (err && err.message !== 'ns not found') return reject(err);

        debug('Collection %s dropped', doc.as);

        removeFromList(callbackToPromise(resolve, reject));
      });
    });
  });
};

Data.prototype.createCollection = function (name) {
  var self = this;

  return new Promise(function (resolve, reject) {
    debug('createCollection %s', name);

    return self.db.collection('collections')
    .findOne({
      fold: self.fold,
      name: name
    }, function (err, doc) {
      if (err) {
        debug(err);

        return reject(err);
      }

      if (doc) return resolve();

      self.db.collection('collections').insert({
        fold: self.fold,
        name: name,
        as: self.fold + '_' + shortid()
      }, function (err) {
        if (err) return reject(err);

        //FIXME: self.announce('collection-create', name);

        return resolve();
      })
    });
  });
};

Data.prototype.insert = function (collection, object) {
  var self = this;

  return new Promise(function (resolve, reject) {
    self.createCollection(collection)
    .then(function () {
      debug('insert into %s', collection);

      self.db.collection('collections').findOne({
        fold: self.fold,
        name: collection
      }, function (err, doc) {
        if (err) return reject(err);

        if (!doc) return reject('Collection does not exists');

        self.db.collection(doc.as).insert(object, function (err, res) {
          if (err) return reject(err);

          //FIXME: self.announce ('collection-insert', collection, res.ops[0]);

          resolve(res.ops[0]);
        });
      });
    });
  });
};

Data.prototype.find = function (collection, predicate, callback) {
  var self = this;

  return new Promise(function (resolve, reject) {
    return self.createCollection(collection)
    .then(function () {
      self.db.collection('collections').findOne({
        fold: self.fold,
        name: collection
      }, function (err, doc) {
        if (err) return reject(err);

        if (!doc) return reject('Collection not found');

        self.db.collection (doc.as)
        .find(predicate)
        .toArray(callbackToPromise(resolve, reject));
      });
    });
  });
};

Data.prototype.findOne = function (collection, predicate, callback) {
  var self = this;
  debug('.findOne')

  return new Promise(function (resolve, reject) {
    return self.createCollection(collection)
    .then(function () {
      self.db.collection('collections').findOne({
        fold: self.fold,
        name: collection
      }, function (err, doc) {
        if (err) return reject(err);

        if (!doc) return reject('Collection not found');

        self.db
        .collection(doc.as)
        .findOne(predicate, callbackToPromise(resolve, reject));
      });
    });
  });
};

Data.prototype.remove = function (collection, predicate) {
  var self = this;

  return new Promise(function (resolve, reject) {
    return self.createCollection(collection)
    .then(function () {
      self.db.collection('collections').findOne({
        fold: self.fold,
        name: collection
      }, function (err, doc) {
        if (err) return reject(err);

        if (!doc) return reject('Collection not found');

        self.db.collection(doc.as).remove(predicate, function (err) {
          if (err) reject(err);

          //FIXME: self.announce('collection-remove', collection, predicate);

          resolve();
        });
      });
    });
  })
};

Data.prototype.update = function (collection, predicate, replacement) {
  var self = this;

  return new Promise(function (resolve, reject) {
    self.createCollection(collection)
   .then(function () {
      return self.db.collection('collections').findOne({
        fold: self.fold,
        name: collection
      }, function (err, doc) {
        if (err) return reject(err);

        if (!doc) return reject('Collection not found');

        self.db.collection(doc.as).update(predicate, replacement, function (err) {
          if (err) reject(err);

          //FIXME: self.announce('collection-update', collection, predicate, replacement);

          resolve();
        });
      });
    });
  });
};

module.exports = Data;

if (module.parent === null) {
  var Plugin = require('origami2-api-plugin/plugin');

  require('mongodb').MongoClient.connect(config.db, function (err, db) {
    if (err) throw err;

    var plugin = new Plugin(Data, {
      db: db
    });

    plugin.connect(config.stack, function (err) {
      if (err) throw err;
    });
  });
}
